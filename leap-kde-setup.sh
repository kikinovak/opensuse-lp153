#!/bin/bash
#
# leap-kde-setup.sh
#
# (c) Niki Kovacs 2021 <info@microlinux.fr>
#
# This script turns a standard OpenSUSE Leap 15.3 KDE installation into a
# full-blown Linux desktop with bells and whistles. 

# OpenSUSE Leap release
VERSION=15.3

# Current directory
CWD=$(pwd)

# Slow things down a bit
SLEEP=1

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then
  echo
  echo "  Please run with sudo or as root." >&2
  echo
  sleep ${SLEEP}
  exit 1
fi

# Make sure we're running OpenSUSE Leap
source /etc/os-release
if [ "${?}" -ne 0 ]
then
  echo 
  echo "  Unsupported operating system." >&2
  echo
  sleep ${SLEEP}
  exit 1
elif [ "${PRETTY_NAME}" != "openSUSE Leap 15.3" ] 
then
  echo 
  echo "  Please run this script on OpenSUSE Leap 15.3 only." >&2
  echo
  sleep ${SLEEP}
  exit 1
fi

echo 
echo "  ########################################"
echo "  # OpenSUSE Leap ${VERSION} KDE configuration #" 
echo "  ########################################"
echo
sleep ${SLEEP}

# Defined users
USERS="$(awk -F: '$3 > 999 {print $1}' /etc/passwd | sort)"

# Remove these packages
CRUFT=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/zypper/useless-packages.txt)

# Additional packages
EXTRA=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/zypper/extra-packages.txt)

# DVD/RW-related packages
DVDRW=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/zypper/dvdrw-packages.txt)

# Logs
LOG="/tmp/$(basename "${0}" .sh).log"
echo > ${LOG}

# Backup configuration
BACKUPS="libreoffice \
         Microsoft \
         ownCloud \
         soffice.binrc
         teams \
         transmission \
         VirtualBox \
         VirtualBox6rc \
         vlc"

# Download mirrors
MIRROR="http://download.opensuse.org"
NVIDIA="http://download.nvidia.com"
PACKMAN="http://ftp.gwdg.de/pub/linux/misc/packman/suse"
DVDCSS="http://opensuse-guide.org/repo"
KDEXTRA="${MIRROR}/repositories/KDE:/Extra"
MOZILLA="${MIRROR}/repositories/mozilla"
DARKTABLE="${MIRROR}/repositories/graphics:/darktable"
VYM="${MIRROR}/repositories/home:/insilmaril"
MICROLINUX="https://www.microlinux.fr/download"

# Open Source Software 
REPONAME[1]="oss"
REPOSITE[1]="${MIRROR}/distribution/leap/${VERSION}/repo/oss"
PRIORITY[1]="99" # standard

# Non Open Source Software 
REPONAME[2]="non-oss"
REPOSITE[2]="${MIRROR}/distribution/leap/${VERSION}/repo/non-oss"
PRIORITY[2]="99" # standard

# Open Source Updates
REPONAME[3]="oss-updates"
REPOSITE[3]="${MIRROR}/update/leap/${VERSION}/oss"
PRIORITY[3]="99" # standard

# Non Open Source Updates 
REPONAME[4]="non-oss-updates"
REPOSITE[4]="${MIRROR}/update/leap/${VERSION}/non-oss"
PRIORITY[4]="99" # standard

# SUSE Linux Enterprise Updates
REPONAME[5]="sle-updates"
REPOSITE[5]="${MIRROR}/update/leap/${VERSION}/sle"
PRIORITY[5]="99" # standard

# Backports Updates
REPONAME[6]="backports-updates"
REPOSITE[6]="${MIRROR}/update/leap/${VERSION}/backports"
PRIORITY[6]="99" # standard

# NVidia 
REPONAME[7]="nvidia"
REPOSITE[7]="${NVIDIA}/opensuse/leap/${VERSION}"
PRIORITY[7]="100" # low

# Multimedia 
REPONAME[8]="packman"
REPOSITE[8]="${PACKMAN}/openSUSE_Leap_${VERSION}"
PRIORITY[8]="90" # high

# DVDCSS
REPONAME[9]="dvdcss"
REPOSITE[9]="${DVDCSS}/openSUSE_Leap_${VERSION}"
PRIORITY[9]="99" # standard

# KDE
REPONAME[10]="kde"
REPOSITE[10]="${KDEXTRA}/openSUSE_Leap_${VERSION}"
PRIORITY[10]="90" # high

# Mozilla
REPONAME[11]="mozilla"
REPOSITE[11]="${MOZILLA}/openSUSE_Leap_${VERSION}"
PRIORITY[11]="90" # high

# Darktable 
REPONAME[12]="darktable"
REPOSITE[12]="${DARKTABLE}/openSUSE_Leap_${VERSION}"
PRIORITY[12]="90" # high

# VYM
REPONAME[13]="vym"
REPOSITE[13]="${VYM}/openSUSE_Leap_${VERSION}"
PRIORITY[13]="90" # high

# Number of repositories
REPOS=${#REPONAME[*]}

usage() {
  # Display help message
  echo "  Usage: ${0} OPTION"
  echo
  echo "  OpenSUSE Leap 15.3 KDE post-install configuration."
  echo
  echo "  Options:"
  echo "    --shell    Configure Bash, Vim and Xterm."
  echo "    --repos    Setup official and third-party repositories."
  echo "    --fresh    Sync repositories and fetch updates."
  echo "    --strip    Remove unneeded applications."
  echo "    --extra    Install extra tools and applications."
  echo "    --fonts    Install Microsoft and Eurostile fonts."
  echo "    --menus    Configure custom menu entries."
  echo "    --kderc    Install custom KDE profile."
  echo "    --users    Apply custom KDE profile for existing users."
  echo "    --setup    Perform all of the above in one go."
  echo "    --dvdrw    Install DVD/RW-related applications."
  echo "    --proxy    Configure local proxy cache for Zypper."
  echo "    --reset    Restore default system configuration."
  echo
  echo "  Logs are written to ${LOG}."
  echo
  sleep ${SLEEP}
}

configure_shell() {
  echo "  === Shell configuration ==="
  echo
  sleep ${SLEEP}
  # Install custom command prompts and a handful of nifty aliases.
  echo "  Configuring Bash shell for user: root"
  cat ${CWD}/bash/root-bashrc > /root/.bashrc
  sleep ${SLEEP}
  echo "  Configuring Bash shell for future users."
  cat ${CWD}/bash/user-alias > /etc/skel/.alias
  sleep ${SLEEP}
  # Add a handful of nifty default options for Vim.
  echo "  Removing obsolete Vim configuration."
  rm -f /etc/vimrc
  sleep ${SLEEP}
  echo "  Configuring Vim editor for user: root"
  cat ${CWD}/vim/vimrc > /root/.vimrc
  sleep ${SLEEP}
  echo "  Configuring Vim editor for future users."
  cat ${CWD}/vim/vimrc > /etc/skel/.vimrc
  sleep ${SLEEP}
  # Make Xterm look less ugly.
  echo "  Configuring Xterm for user: root"
  cat ${CWD}/xterm/Xresources > /root/.Xresources
  sleep ${SLEEP}
  echo "  Configuring Xterm for future users."
  cat ${CWD}/xterm/Xresources > /etc/skel/.Xresources
  sleep ${SLEEP}
  # Existing users might want to use it.
  if [ ! -z "${USERS}" ]
  then
    for USER in ${USERS}
    do
      if [ -d /home/${USER} ]
      then
        echo "  Configuring Bash shell for user: ${USER}"
        cat ${CWD}/bash/user-alias > /home/${USER}/.alias
        chown ${USER}:users /home/${USER}/.alias
        sleep ${SLEEP}
        echo "  Configuring Vim editor for user: ${USER}"
        cat ${CWD}/vim/vimrc > /home/${USER}/.vimrc
        chown ${USER}:users /home/${USER}/.vimrc
        sleep ${SLEEP}
        echo "  Configuring Xterm for user: ${USER}"
        cat ${CWD}/xterm/Xresources > /home/${USER}/.Xresources
        chown ${USER}:users /home/${USER}/.Xresources
        sleep ${SLEEP}
      fi
    done
  fi
  echo
}

configure_repos() {
  echo "  === Package repository configuration ==="
  echo
  sleep ${SLEEP}
  echo "  Removing existing repositories."
  rm -f /etc/zypp/repos.d/*.repo*
  sleep ${SLEEP}
  for (( REPO=1 ; REPO<=${REPOS} ; REPO++ ))
  do
    echo "  Configuring repository: ${REPONAME[${REPO}]}"
    zypper addrepo --keep-packages --priority ${PRIORITY[${REPO}]} \
      ${REPOSITE[${REPO}]} ${REPONAME[${REPO}]} >> ${LOG} 2>&1
    if [ "${?}" -ne 0 ]
    then
      echo "  Could not add repository: ${REPONAME[${REPO}]}" >&2
      exit 1
    fi
    sleep ${SLEEP}
  done
  # NVidia repository is broken
  # echo "gpgcheck=off" >> /etc/zypp/repos.d/nvidia.repo --> FIXED!
  # AnyDesk Remote Desktop
  # http://rpm.anydesk.com/howto.html
  echo "  Configuring repository: anydesk"
  zypper addrepo --keep-packages --priority 100 --repo \
    ${CWD}/anydesk/anydesk.repo >> ${LOG} 2>&1
  sleep ${SLEEP}
  echo
}

configure_proxy() {
  read -p "  Enter proxy server hostname [proxy]: " PROXY_NAME 
  PROXY_NAME=${PROXY_NAME:-proxy}
  read -p "  Enter proxy server port [3142]: " PROXY_PORT 
  PROXY_PORT=${PROXY_PORT:-3142}
  PROXY="${PROXY_NAME}:${PROXY_PORT}"
  echo
  sleep ${SLEEP}
  configure_repos
  # Official repositories only
  echo "  === Proxy cache server configuration ==="
  echo
  sleep ${SLEEP}
  for REPO in oss oss-updates \
              non-oss non-oss-updates \
              sle-updates backports-updates
  do
    echo "  Using local proxy for repository: ${REPO}"
    sed -i "s/baseurl=http:\/\//baseurl=http:\/\/${PROXY}\//g" \
      /etc/zypp/repos.d/${REPO}.repo
    sleep ${SLEEP}
  done
  echo
}

update_system() {
  echo "  === Update system ==="
  echo
  sleep ${SLEEP}
  # Refresh metadata and import GPG keys.
  echo "  Refreshing repository information."
  sleep ${SLEEP}
  echo "  This might take a moment..."
  zypper --gpg-auto-import-keys refresh >> ${LOG} 2>&1
  if [ "${?}" -ne 0 ]
  then
    echo "  Could not refresh repository information." >&2
    exit 1
  fi
  # Update and replace some vanilla packages with enhanced versions.
  echo "  Updating system with enhanced packages."
  sleep ${SLEEP}
  echo "  This might also take a moment..."
  zypper --non-interactive dist-upgrade --allow-vendor-change \
    --auto-agree-with-licenses --no-recommends --replacefiles >> ${LOG} 2>&1
  if [ "${?}" -ne 0 ]
  then
    echo "  Could not perform system update." >&2
    echo
    exit 1
  fi
  echo
  replace_menus 
}

strip_system() {
  # Remove unneeded applications listed in zypper/useless-packages.txt.
  echo "  === Remove useless packages ==="
  echo
  sleep ${SLEEP}
  for PACKAGE in ${CRUFT}
  do
    if rpm -q ${PACKAGE} > /dev/null 2>&1 
    then
      echo "  Removing package: ${PACKAGE}"
      zypper --non-interactive remove --clean-deps ${PACKAGE} >> ${LOG} 2>&1
      if [ "${?}" -ne 0 ]
        then
        echo "  Could not remove package ${PACKAGE}." >&2
        echo
        exit 1
      fi
    fi
  done
  echo "  All useless packages removed from the system."
  echo
  sleep ${SLEEP}
}

install_extra() {
  # Install extra packages listed in zypper/extra-packages.txt.
  echo "  === Install extra packages ==="
  echo
  sleep ${SLEEP}
  for PACKAGE in ${EXTRA}
  do
    if ! rpm -q ${PACKAGE} > /dev/null 2>&1 
    then
      echo "  Installing package: ${PACKAGE}"
      zypper --non-interactive install --no-recommends \
        --allow-vendor-change ${PACKAGE} >> ${LOG} 2>&1
      if [ "${?}" -ne 0 ]
        then
        echo "  Could not install package ${PACKAGE}." >&2
        echo
        exit 1
      fi
    fi
  done
  echo "  All extra packages installed on the system."
  echo 
  sleep ${SLEEP}
}

install_dvdrw() {
  # Install DVD/RW-related packages listed in zypper/dvdrw-packages.txt.
  echo "  === Install DVD/RW-related applications ==="
  echo
  sleep ${SLEEP}
  for PACKAGE in ${DVDRW}
  do
    if ! rpm -q ${PACKAGE} > /dev/null 2>&1 
    then
      echo "  Installing package: ${PACKAGE}"
      zypper --non-interactive install --no-recommends \
        --allow-vendor-change ${PACKAGE} >> ${LOG} 2>&1
      if [ "${?}" -ne 0 ]
        then
        echo "  Could not install package ${PACKAGE}." >&2
        echo
        exit 1
      fi
    fi
  done
  echo "  All DVD/RW-related packages installed on the system."
  echo 
  sleep ${SLEEP}
}

install_fonts() {
  echo "  === Install additional TrueType fonts ==="
  echo
  sleep ${SLEEP}
  # Download and install Microsoft TrueType fonts.
  if [ ! -d /usr/share/fonts/truetype/microsoft ]
  then
    pushd /tmp >> ${LOG} 2>&1
    rm -rf /usr/share/fonts/truetype/microsoft
    rm -rf /usr/share/fonts/truetype/msttcorefonts
    echo "  Installing Microsoft TrueType fonts."
    wget -c --no-check-certificate \
      ${MICROLINUX}/webcore-fonts-3.0.tar.gz >> ${LOG} 2>&1 \
    wget -c --no-check-certificate \
      ${MICROLINUX}/symbol.gz >> ${LOG} 2>&1
    mkdir /usr/share/fonts/truetype/microsoft
    tar xvf webcore-fonts-3.0.tar.gz >> ${LOG} 2>&1
    pushd webcore-fonts >> ${LOG} 2>&1
    if type fontforge > /dev/null 2>&1
    then
      fontforge -lang=ff -c 'Open("vista/CAMBRIA.TTC(Cambria)"); \
        Generate("vista/CAMBRIA.TTF");Close();Open("vista/CAMBRIA.TTC(Cambria Math)"); \
        Generate("vista/CAMBRIA-MATH.TTF");Close();' >> ${LOG} 2>&1
      rm vista/CAMBRIA.TTC
    fi
    cp fonts/* /usr/share/fonts/truetype/microsoft/
    cp vista/* /usr/share/fonts/truetype/microsoft/
    popd >> ${LOG} 2>&1
    fc-cache -f -v >> ${LOG} 2>&1
  fi
  # Remove obsolete Apple TrueType fonts
  if [ -d /usr/share/fonts/apple-fonts ]
  then
    echo "  Removing obsolete Apple TrueType fonts."
    rm -rf /usr/share/fonts/apple-fonts
    fc-cache -f -v >> ${LOG} 2>&1
  fi
  # Download and install Eurostile fonts
  if [ ! -d /usr/share/fonts/eurostile ]
  then
    cd /tmp
    rm -rf /usr/share/fonts/eurostile
    echo "  Installing Eurostile TrueType fonts."
    wget -c --no-check-certificate ${MICROLINUX}/Eurostile.zip >> ${LOG} 2>&1
    unzip Eurostile.zip -d /usr/share/fonts/ >> ${LOG} 2>&1
    mv /usr/share/fonts/Eurostile /usr/share/fonts/eurostile
    fc-cache -f -v >> ${LOG} 2>&1
    rm -f Eurostile.zip
    cd - >> ${LOG} 2>&1
  fi
  echo "  Additional TrueType fonts installed on the system."
  echo
  sleep ${SLEEP}
}

replace_menus() {
  # Install custom menu entries with enhanced translations.
  ENTRIESDIR="${CWD}/menus"
  ENTRIES=$(ls ${ENTRIESDIR})
  MENUDIRS="/usr/share/applications" 
  echo "  === Install custom desktop menu ==="
  echo
  for MENUDIR in ${MENUDIRS}
  do
    for ENTRY in ${ENTRIES}
    do
      if [ -r ${MENUDIR}/${ENTRY} ]
      then
        echo "  Installing menu item: ${ENTRY}"
        cat ${ENTRIESDIR}/${ENTRY} > ${MENUDIR}/${ENTRY}
        sleep 0.2
      fi
    done
  done
  echo "  Updating desktop database."
  update-desktop-database
  sleep ${SLEEP}
  echo "  Custom desktop menu installed."
  sleep ${SLEEP}
  echo
}

install_profile() {
  echo "  === Configure KDE desktop environment ==="
  echo 
  sleep ${SLEEP}
  echo "  Removing existing profile."
  rm -rf /etc/skel/.config
  mkdir /etc/skel/.config
  sleep ${SLEEP}
  echo "  Installing custom KDE profile."
  cp -v ${CWD}/kde/* /etc/skel/.config/ >> ${LOG} 2>&1
  sleep ${SLEEP}
  echo
}

restore_profile() {
  echo "  === Apply custom KDE configuration ==="
  echo
  sleep ${SLEEP}
  if [ ! -d /etc/skel/.config ]
  then
    echo "  Custom profiles are not installed." >&2
    echo
    exit 1
  fi
  for USER in ${USERS}
  do
    if [ -d /home/${USER} ] 
    then
      echo "  Updating profile for user: ${USER}"
      rm -rf /home/${USER}/.config.bak
      mv -f /home/${USER}/.config /home/${USER}/.config.bak
      cp -R /etc/skel/.config /home/${USER}/
      for BACKUP in ${BACKUPS}
      do
        if [ -r /home/${USER}/.config.bak/${BACKUP} ]
        then 
          cp -R /home/${USER}/.config.bak/${BACKUP} /home/${USER}/.config/
        fi
      done
      chown -R ${USER}:users /home/${USER}/.config
      sleep ${SLEEP}
    fi
  done
  echo
}

reset_system() {
  echo "  === Restore default configuration ==="
  echo
  sleep ${SLEEP}
  # Remove all extra applications.
  # Useful before upgrading to the next version.
  echo "  Removing extra packages from the system."
  for PACKAGE in ${CRUFT} ${EXTRA} ${DVDRW}
  do
    if rpm -q ${PACKAGE} > /dev/null 2>&1 
    then
      echo "  Removing package: ${PACKAGE}"
      zypper --non-interactive remove --clean-deps ${PACKAGE} >> ${LOG} 2>&1
      # Don't check for errors here.
    fi
  done
  echo "  All extra packages removed from the system."
  sleep ${SLEEP}
  # Configure official repositories only.
  echo "  Removing existing repositories."
  rm -f /etc/zypp/repos.d/*.repo
  sleep ${SLEEP}
  echo "  Configuring official repositories only."
  sleep ${SLEEP}
  for (( REPO=1 ; REPO<=6 ; REPO++ ))
  do
    echo "  Configuring repository: ${REPONAME[${REPO}]}"
    zypper addrepo --priority ${PRIORITY[${REPO}]} \
      ${REPOSITE[${REPO}]} ${REPONAME[${REPO}]} >> ${LOG} 2>&1
    if [ "${?}" -ne 0 ]
    then
      echo "  Could not add repository: ${REPONAME[${REPO}]}" >&2
      echo
      exit 1
    fi
    sleep ${SLEEP}
  done
  # Refresh metadata and import GPG keys.
  echo "  Refreshing repository information."
  sleep ${SLEEP}
  echo "  This might take a moment..."
  zypper --gpg-auto-import-keys refresh >> ${LOG} 2>&1
  if [ "${?}" -ne 0 ]
  then
    echo "  Could not refresh repository information." >&2
    echo
    exit 1
  fi
  # Replace enhanced packages with vanilla counterparts.
  echo "  Updating system with official packages."
  sleep ${SLEEP}
  echo "  This might also take a moment..."
  zypper --non-interactive dist-upgrade --allow-vendor-change >> ${LOG} 2>&1
  if [ "${?}" -ne 0 ]
  then
    echo "  Could not perform system update." >&2
    echo
    exit 1
  fi
  echo
}

# Check parameters.
if [[ "${#}" -ne 1 ]]
then
  usage
  exit 1
fi
OPTION="${1}"
case "${OPTION}" in
  --shell) 
    configure_shell
    ;;
  --repos)
    configure_repos
    ;;
  --proxy)
    configure_proxy
    ;;
  --fresh)
    update_system
    ;;
  --strip) 
    strip_system
    ;;
  --extra)
    install_extra
    ;;
  --dvdrw)
    install_dvdrw
    ;;
  --fonts) 
    install_fonts
    ;;
  --menus) 
    replace_menus
    ;;
  --kderc) 
    install_profile
    ;;
  --users) 
    restore_profile
    ;;
  --setup)
    configure_shell
    configure_repos
    update_system
    strip_system
    install_extra
    install_fonts
    replace_menus
    install_profile
    restore_profile
    ;;
  --reset) 
    reset_system
    ;;
  --help) 
    usage
    exit 0
    ;;
  ?*) 
    usage
    exit 1
esac

exit 0

