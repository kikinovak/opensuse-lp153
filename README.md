# Post-installation setup script for OpenSUSE Leap 15.3 KDE

(c) Niki Kovacs 2021 

This repository provides an "automagic" post-installation setup script for
OpenSUSE Leap 15.3 KDE. 

## The perfect Linux desktop in a nutshell

Perform the following steps.

  1. Install OpenSUSE Leap 15.3 KDE.

  2. Open a terminal (Konsole) as root (`su -`).

  3. Install Git: `zypper install --no-recommends git`

  4. Grab the script: `git clone https://gitlab.com/kikinovak/opensuse-lp153`

  5. Change into the new directory: `cd opensuse-lp153`

  6. Run the script: `./leap-kde-setup.sh --setup`

  7. Grab a cup of coffee while the script does all the work.

  8. Log out and log back in.

## Customizing a Linux desktop

Turning a vanilla Linux installation into a full-blown desktop with bells and
whistles always boils down to a series of more or less time-consuming
operations:

  * Customize the Bash shell: prompt, aliases, etc.

  * Customize the Vim editor.

  * Setup official and third-party repositories.

  * Remove some unneeded applications.

  * Install all missing applications.

  * Enhance multimedia capabilities with various codecs and plugins.

  * Install Microsoft and Eurostile fonts for better interoperability.

  * Edit application menu entries.

  * Configure the KDE desktop for better usability.

The `leap-kde-setup.sh` script performs all of these operations and some more.

Configure Bash, Vim and Xterm:
```
# ./leap-kde-setup.sh --shell
```
Setup official and third-party repositories:
```
# ./leap-kde-setup.sh --repos
```
Update system with enhanced packages and clean menus:
```
# ./leap-kde-setup.sh --fresh
```
Remove unneeded applications:
```
# ./leap-kde-setup.sh --strip
```
Install extra tools and applications:
```
# ./leap-kde-setup.sh --extra
```
Install Microsoft and Eurostile fonts:
```
# ./leap-kde-setup.sh --fonts
```
Configure custom menu entries:
```
# ./leap-kde-setup.sh --menus
```
Install custom KDE profile:
```
# ./leap-kde-setup.sh --kderc
```
Apply custom KDE profile for existing users:
```
# ./leap-kde-setup.sh --users
```
Perform all of the above in one go:
```
# ./leap-kde-setup.sh --setup
```
Install DVD/RW-related applications:
```
# ./leap-kde-setup.sh --dvdrw
```
Configure local proxy cache for Zypper:
```
# ./leap-kde-setup.sh --proxy
```
Restore default system configuration:
```
# ./leap-kde-setup.sh --reset
```
Display an overview of all available options:
```
# ./leap-kde-setup.sh --help
```
If you want to know what exactly goes on under the hood, open a second terminal
and view the logs:
```
$ tail -f /tmp/leap-kde-setup.log
```

---

*[Click here](https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG)
to buy me a cup of coffee.*

